// GENERATED AUTOMATICALLY FROM 'Assets/LogoControl.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @LogoControl : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @LogoControl()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""LogoControl"",
    ""maps"": [
        {
            ""name"": ""Logo"",
            ""id"": ""dd201264-99f2-4a5e-94d0-f79d9eda73b5"",
            ""actions"": [
                {
                    ""name"": ""Anykey1"",
                    ""type"": ""PassThrough"",
                    ""id"": ""2085265f-8b27-4bef-a9e7-3391f5c049c0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b815e067-5593-4af9-b774-ffa92160cd4a"",
                    ""path"": ""<Keyboard>/anyKey"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Anykey1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Logo
        m_Logo = asset.FindActionMap("Logo", throwIfNotFound: true);
        m_Logo_Anykey1 = m_Logo.FindAction("Anykey1", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Logo
    private readonly InputActionMap m_Logo;
    private ILogoActions m_LogoActionsCallbackInterface;
    private readonly InputAction m_Logo_Anykey1;
    public struct LogoActions
    {
        private @LogoControl m_Wrapper;
        public LogoActions(@LogoControl wrapper) { m_Wrapper = wrapper; }
        public InputAction @Anykey1 => m_Wrapper.m_Logo_Anykey1;
        public InputActionMap Get() { return m_Wrapper.m_Logo; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(LogoActions set) { return set.Get(); }
        public void SetCallbacks(ILogoActions instance)
        {
            if (m_Wrapper.m_LogoActionsCallbackInterface != null)
            {
                @Anykey1.started -= m_Wrapper.m_LogoActionsCallbackInterface.OnAnykey1;
                @Anykey1.performed -= m_Wrapper.m_LogoActionsCallbackInterface.OnAnykey1;
                @Anykey1.canceled -= m_Wrapper.m_LogoActionsCallbackInterface.OnAnykey1;
            }
            m_Wrapper.m_LogoActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Anykey1.started += instance.OnAnykey1;
                @Anykey1.performed += instance.OnAnykey1;
                @Anykey1.canceled += instance.OnAnykey1;
            }
        }
    }
    public LogoActions @Logo => new LogoActions(this);
    public interface ILogoActions
    {
        void OnAnykey1(InputAction.CallbackContext context);
    }
}
