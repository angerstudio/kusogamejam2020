﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System.Linq;

[System.Serializable]
public class ItemSet
{
    public List<Sprite> sprites;
    public string id;
    public VideoClip result_clip;
    public Sprite result_sprite;
    public AudioClip result_audio;
}

public class ItemManager : MonoBehaviour
{
    [SerializeField]
    List<ItemSet> m_set;

    Dictionary<string,ItemSet> _itemSet;
    public Dictionary<string, ItemSet> itemSet {
        get 
        {
            if (_itemSet == null)
            {
                _itemSet = new Dictionary<string, ItemSet>();
                foreach(var set in m_set)
                    _itemSet.Add(set.id, set);
            }

            return _itemSet;
        }
    }

    [SerializeField]
    Transform Contailer;

    [SerializeField]
    Vector2 itemSize;

    // Start is called before the first frame update
    void Start()
    {
        SpreadObjects();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpreadObjects()
    {
        var contailers = Contailer.Cast<Transform>().ToList();

        //Shuffle the list
        int n = contailers.Count;
        var rng = new System.Random();
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            var value = contailers[k];
            contailers[k] = contailers[n];
            contailers[n] = value;
        }

        int i = 0;
        foreach(var obj in m_set)
        {
            foreach (var sp in obj.sprites)
            {
                var c = contailers[i++];
                var sr = c.GetComponent<SpriteRenderer>();
                sr.sprite = sp;
                sr.Resize((int)itemSize.x, (int)itemSize.y);
                c.name = obj.id;
            }
        }
    }
}
