﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Core : MonoBehaviour
{
    public static Core Singleton { get; set; }
    
    int twig;
    public int Twig
    {
        get => twig;
        set
        {
            twig = value;
            UpdateTwigUI();
        }
    }
    public int twigTarget = 99;
    public TMPro.TextMeshProUGUI text;

    public bool pauseInput = false;

    public UnityEngine.UI.RawImage failRender;
    public VideoPlayer failPlayer;

    public GameObject SuccessGB;


    [SerializeField]
    MemePlayer memePlayer;
    [SerializeField]
    ItemManager itemManager;
    [SerializeField]
    CombineEventTrigger combineEventTrigger;

    void Awake()
    {
        Singleton = this;
        combineEventTrigger.SetItems(itemManager.itemSet);
        combineEventTrigger.SetOnCombined(OnResultReceived);
    }

    void Start ()
    {
        UpdateTwigUI();
    }

    void UpdateTwigUI ()
    {
        text.text = Twig + " / " + twigTarget;
    }

    [SerializeField]
    Timer timer;
    void OnResultReceived(ResultObject _result)
    {
        memePlayer.onResultReceived(_result);
        Twig +=  _result.spriteCount;
        timer.totalTime += _result.spriteCount;
        // pauseInput = true;
        // StartCoroutine(DoAfter(1, () => pauseInput = false));
    }

    IEnumerator DoAfter (float second, System.Action callback)
    {
        yield return new WaitForSeconds(second);
        callback();
    }

    public void OnTimesUp ()
    {
        if (Twig < twigTarget)
        {
            Fail();
        }
        else
        {
            Success();
        }
    }

    public void Fail ()
    {
        failRender.gameObject.SetActive(true);
        failPlayer.Play();
    }

    public void Success()
    {
        SuccessGB.SetActive(true);
    }

    public void Reload ()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
    }
}
