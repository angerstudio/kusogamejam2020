﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Success : MonoBehaviour
{
    public Text t1,t2,t3,t4;
    public GameObject replay;

    void OnEnable()
    {
        t1.color = new Color(1,1,1,0);
        t2.color = new Color(1, 1, 1, 0);
        t3.color = new Color(1, 1, 1, 0);
        t4.color = new Color(1, 1, 1, 0);
        replay.SetActive(false);

        IEnumerator TextFade()
        {
            t1.DOFade(1, 2);
            yield return new WaitForSeconds(2);
            t2.DOFade(1, 2);
            yield return new WaitForSeconds(2);
            t3.DOFade(1, 2);
            yield return new WaitForSeconds(2);
            t4.DOFade(1, 2);
            yield return new WaitForSeconds(2);
            replay.SetActive(true);
        }

        StartCoroutine(TextFade());
    }
}
