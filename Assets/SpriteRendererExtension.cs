using System.Collections;
using System.Collections.Generic;
using UnityEngine;
static class SpriteRendererExtension
{
    public static void Resize(this SpriteRenderer sr, int width, int height)
    {
        var t = sr.transform;
        var size = sr.sprite.rect.size;
        t.localScale = new Vector2(
            t.localScale.x * (width / size.x),
            t.localScale.y * (height / size.y)
        );
    }
}