﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BA_Studio.UnityLib.Caster2D;
using UnityEngine.Events;


public class Picker : MonoBehaviour
{
    PhysicsCaster2DV2 caster2D;

    public BoolEvent onPickableChange;
    
    public StringEvent onPlayerPick;
    public GameObjectEvent onPlayerPickGO;

    CharacterInputAction inputAction;

    bool pickable;

    GameObject lastCasted;

    int skipping = 0;

    void Awake ()
    {
        inputAction = new CharacterInputAction();
        inputAction.Character.Pick.performed += ctx => {
            if (pickable && lastCasted != null)
            {
                Debug.Log("Picking " + lastCasted.name);
                onPlayerPick.Invoke(lastCasted.name);
                onPlayerPickGO.Invoke(lastCasted);
                lastCasted.SetActive(false);
            }
        };
    }

    void Start ()
    {
        caster2D = this.GetComponent<PhysicsCaster2DV2>();
        pickable = CheckPickable();
        onPickableChange.Invoke(pickable);
    }

    void Update ()
    {
        skipping++;
        if (skipping >= 3)
        { 
            CheckPickable();
            skipping = 0;
        }
    }

    bool CheckPickable ()
    {
        Collider2D c = caster2D.Cast();
        if (c != null)
        {
            if (pickable != true) onPickableChange.Invoke(true);
            pickable = true;
            lastCasted = c.gameObject;
            return true;
        }
        else
        {
            if (pickable != false) onPickableChange.Invoke(false);
            pickable = false;
            lastCasted = null;
            return false;
        }
    }

    void OnEnable ()
    {
        inputAction.Enable();
    }

    void OnDisable()
    {
        inputAction.Disable();
    }
}
