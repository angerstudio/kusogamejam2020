﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    float startTime;

    public float totalTime = 60;

    public TMPro.TextMeshProUGUI text;

    public UnityEngine.Events.UnityEvent onTimesUp;

    bool startCount = false;
    public void StartTimer ()
    {
        startTime = Time.time;
        startCount = true;
    }

    void Update ()
    {
        if (!startCount) return;

        var nowTime = startTime + totalTime - Time.time;
        if (nowTime < 0 )
        {
            startCount = false;
            onTimesUp.Invoke();
        }
        else
        {
            text.text = nowTime.ToString("F1");
        }
    }
}
