﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;


public enum ResultType
{
    VIDEO,
    SPRITE,
    SPRITE_SOUND
}

public struct ResultObject
{
    public ResultType type;
    public VideoClip clip;
    public Sprite sprite;
    public AudioClip audio;
    public int spriteCount;

    public ResultObject(ItemSet set)
    {
        if (set.result_clip != null)
        {
            type = ResultType.VIDEO;
        }
        else if (set.result_audio != null)
        {
            type = ResultType.SPRITE_SOUND;
        }
        else
        {
            type = ResultType.SPRITE;
        }
        clip = set.result_clip;
        audio = set.result_audio;
        sprite = set.result_sprite;
        spriteCount = set.sprites.Count;
    }
}

public class CombineEventTrigger : MonoBehaviour
{


    System.Action<ResultObject> m_onCombined;
    Dictionary<string, int> numberMap;
    Dictionary<string, ItemSet> m_itemMap;

    public void SetItems(Dictionary<string, ItemSet> itemMap)
    {
        m_itemMap = itemMap;
        numberMap = new Dictionary<string, int>();
        foreach (var set in itemMap)
        {
            numberMap.Add(set.Key, set.Value.sprites.Count);
        }
    }

    public void SetOnCombined(System.Action<ResultObject> _onCombined)
    {
        m_onCombined = _onCombined;
    }

    public void Receive(string _iSourse)
    {
        if (!numberMap.ContainsKey(_iSourse)) return;

        numberMap[_iSourse]--;
        if (numberMap[_iSourse] <= 0)
        {
            m_onCombined(new ResultObject(
                m_itemMap[_iSourse]
            ));
        }
    }
}
