﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    CharacterInputAction inputAction;

    Vector2 movementInput;

    public float moveSpeed = 1.5f;

    void Awake ()
    {
        inputAction = new CharacterInputAction();
        inputAction.Character.Move.performed += ctx => movementInput = ctx.ReadValue<Vector2>();
    }

    void Update ()
    {
        if (!Core.Singleton.pauseInput) this.transform.position = ((Vector2) this.transform.position + movementInput * Time.deltaTime * moveSpeed);
    }

    void OnEnable ()
    {
        inputAction.Enable();
    }

    void OnDisable ()
    {
        inputAction.Disable();
    }

}
