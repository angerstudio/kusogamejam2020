﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BreathingFading : MonoBehaviour
{
    public Image target;
    void Start ()
    {
        target.DOFade(0, 0.6f).SetOptions(true).SetLoops(-1, LoopType.Yoyo);
    }
}
