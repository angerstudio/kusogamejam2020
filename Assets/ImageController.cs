﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageController : MonoBehaviour
{
    public UnityEngine.UI.Image target;

    public void SetImage (Sprite image)
    {
        target.sprite = image;
    }
}
