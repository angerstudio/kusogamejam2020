﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BA_Studio.UnityLib.General;


namespace BA_Studio.UnityLib.Caster2D
{
	public class RayCaster2D : PhysicsCaster2DV2
	{

		public BasicDirections castDirection;

		public float castRange = 0.025f, rangeMin, rangeMax; 

		float responsiveRange, lastSuccededDistance;

		// bool responsiveSucceededLast;

		public bool responsiveCastRange;

		public List<Vector2> castOffsets;

		// Vector2[] RotatedCastOffsets
		// {
		// 	get
		// 	{
		// 		if (!rotateWithTransform) return castOffsets.ToArray();
		// 		else
		// 		{
		// 			List<Vector2> temp = new List<Vector2>(castOffsets);
		// 			foreach (Vector2 v in temp)
		// 			{

		// 			}
		// 		}
		// 	}

		// }

		public bool rotateWithTransform;

		public Vector2 castDirecitonOverride;

		List<Transform> pointGOs;

		void Awake () {

		}

		public override void Start () {
			base.Start();
		}

		public override void Update () {
			base.Update();
			// if (!rotateWithTransform) {
			// 	for (int i = 0; i < pointGOs.Count; i++)
			// 	{
			// 		pointGOs[i].position = baseTransform.position + (Vector3) castOffsets[i] + (Vector3) CastBaseOffsetFlipped;
			// 	}
			// }
		}

		public void OnEnable ()
		{
			// SetupPointGOs();
		}

		public void SetOffsets (Vector2[] offsets)
		{
			this.castOffsets = offsets.ToList();
			SetupPointGOs();
		}

		void SetupPointGOs ()
		{		
			if (pointGOs == null) pointGOs = new List<Transform>();
			while (pointGOs.Count < castOffsets.Count)
			{				
				pointGOs.Add(new GameObject("CastPoint").transform);
				pointGOs.Last().SetParent(baseTransform);
				pointGOs.Last().hideFlags = HideFlags.HideInHierarchy;
			}
			while (pointGOs.Count > castOffsets.Count)
			{
				DestroyImmediate(pointGOs.Last());
				pointGOs.RemoveAt(pointGOs.Count - 1);
			}
			for (int i = 0; i < castOffsets.Count; i++)
			{
				pointGOs[i].localPosition = castOffsets[i];
			}
		}

		public void OnDisable () {
			if (pointGOs.Count > 0) pointGOs.ForEach(e => { if (e != null) Destroy(e.gameObject); });
		}

		#if UNITY_EDITOR
		void OnDrawGizmos () {
			if (!this.enabled) return;
			if (!ToggleVisualization) return;
			// if (UnityEditor.EditorApplication.isPlaying)
			// {
			// 	for (int i = 0; i < pointGOs.Count; i++)
			// 	{
			// 		Gizmos.DrawCube(pointGOs[i].position, Vector3.one * 0.1f);
			// 		Gizmos.DrawLine(
			// 			pointGOs[i].position,
			// 			pointGOs[i].position + ((Vector3) (rotateWithTransform?
			// 				(Vector2) transform.TransformDirection(CommonLibUtil.DirectionToVector(castDirection)) : ((castDirection == BasicDirections.Custom)? 
			// 					castDirecitonOverride : CommonLibUtil.DirectionToVector(castDirection))).normalized) * (responsiveCastRange? responsiveRange : castRange));
			// 		//Gizmos.DrawRay(new Ray(pointGOs[i].position, castDirectionBaked));
			// 	}
				
			// }
			// else {
			// 	if (baseTransform != null) for (int i = 0; i < castOffsets.Count; i++) {
			// 		Gizmos.DrawCube(baseTransform.position + (Vector3) castOffsets[i] + (Vector3) castBaseOffset, Vector3.one * 0.1f);
			// 		Gizmos.DrawLine(baseTransform.position + (Vector3) castOffsets[i] + (Vector3) castBaseOffset,
			// 						baseTransform.position + (Vector3) castOffsets[i] + (Vector3) castBaseOffset + ((Vector3) (rotateWithTransform?
			// 																												(Vector2) baseTransform.TransformDirection(CommonLibUtil.DirectionToVector(castDirection)) : ((castDirection == BasicDirections.Custom)? 
			// 																																																		castDirecitonOverride : CommonLibUtil.DirectionToVector(castDirection))).normalized) * (responsiveCastRange? rangeMax : castRange));
			// 	}
			// }
			// for (int i = 0; i < pointGOs.Count; i++)
			// 	{
			// 		Gizmos.DrawCube(pointGOs[i].position, Vector3.one * 0.1f);
			// 		//Gizmos.DrawRay(new Ray(pointGOs[i].position, castDirectionBaked));
			// 	}
			foreach (Vector2 v in CastPointCoords)
			{
				Gizmos.DrawCube(v, Vector3.one * 0.1f);
				Gizmos.DrawLine(
					v,
					v + CastDirBakedNormalized * castRange);
			}
		}
		#endif

		protected override void PreCast ()
		{
			// if (responsiveCastRange) {
			// 	responsiveRange = responsiveSucceededLast? lastSuccededDistance : responsiveRange*1.2f;
			// 	responsiveRange = Mathf.Clamp(responsiveRange, rangeMin, rangeMax);
			// }
		}

		public Vector2[] CastPointCoords
		{
			get
			{
				// #if UNITY_EDITOR
				Vector2[] v = new Vector2[castOffsets.Count];				
				if (!rotateWithTransform) for (int i = 0; i < castOffsets.Count; i++) v[i] = baseTransform.position + (Vector3) castOffsets[i] + (Vector3) CastBaseOffsetFlipped;
				else
				{					
					for (int i = 0; i < castOffsets.Count; i++)
					{
						v[i] = castOffsets[i] + CastBaseOffsetFlipped;
						if (flipX) v[i].x *= -1;
						if (flipY) v[i].y *= -1;
						v[i] = baseTransform.position + Quaternion.Euler(0, 0, baseTransform.localRotation.eulerAngles.z) * v[i];
					}
				}
				return v;
				// #endif
				// return pointGOs.Select(p => (Vector2) p.transform.position).ToArray();
			}
		}

		/// <summary>
		/// Do the cast and store the filtered results (BlackList, BlackTags) into Cache. If OnlyTarget is true the results will be all casted and filtered with target tag.
		/// </summary>
		/// <returns>Did the cast succeed and there are more then 1 result in Cache.</returns>
		protected override void DoCast () {
			PreCast();

			hits = null;

			if (cache == null) cache = new List<RaycastHit2D>();
			else cache.Clear();

			foreach (Vector2 p in CastPointCoords) {

				hits = new RaycastHit2D[maxCandidate];

				if (filter2D.useDepth)
					Physics2D.RaycastNonAlloc(
						p,
						CastDirBakedNormalized,
						hits,
						castRange,
						(filter2D.useLayerMask)? filter2D.layerMask : (LayerMask) System.Int32.MaxValue,
						filter2D.minDepth,
						filter2D.maxDepth);
				else Physics2D.RaycastNonAlloc(
					p,
					CastDirBakedNormalized,
					hits,
					castRange,
					(filter2D.useLayerMask)? filter2D.layerMask : (LayerMask) System.Int32.MaxValue);	
				// if (ID == "GroundCheck") Debug.Log(ID + " (Raw) From: " + t.transform.position + ", RayCastHits(" + hits.Length +"): " + ArrayToString(hits));

				if (hits.All(e => !e)) continue;

				// if (ID == "GroundCheck")  Debug.Log(ID + "From: " + t.transform.position + ", RayCastHits(" + hits.Length +"): " + ArrayToString(hits));

				//Iterate through the blacklists.
				cache.AddRange(hits);

				cache.RemoveAll(e => e.collider == null);
				if (!filter2D.useTriggers) cache.RemoveAll(e => e.collider.isTrigger);
				cache.RemoveAll(e => blackList.Contains(e.collider));
				if (whiteListMode) cache.RemoveAll(e => !targetTags.Contains(e.collider.tag));
				else {
					cache.RemoveAll(e => ignoreTags.Contains(e.collider.tag));	
				}

				// if (ID == "GroundCheck")  Debug.Log(ID + "FiteredCache(" + cache.Count +"): " + ArrayToString(cache.ToArray()));
			}

			DoneCast();
		}

		protected override void DoneCast ()
		{
			base.DoneCast();
			if (oneShot_CastRange) castRange = castRange1ShotCache;
		}

		float castRange1ShotCache;

		bool oneShot_CastRange;

		public void SetCastRange (float Range, bool OneShot = false)
		{
			if (responsiveCastRange) Debug.Log("RayCaster2D:: SetCastRange():: The raycaster is in responsive mode, SetCastRange() will not work.");
			if (OneShot) {
				castRange1ShotCache = castRange;
				oneShot_CastRange = true;
			}
			castRange = Range;
		}

		string ArrayToString (RaycastHit2D[] array) {
			string temp = "";
			if (array == null) return temp;
			for (int i = 0; i < array.Length; i++) if (array[i]) temp += (i == 0? "":", ") + array[i].collider.name;
			return temp;
		}

		List<RaycastHit2D> cache, targetCache, utilList;

		Vector2 castDirectionBaked;

		public Vector2 CastDirBakedNormalized
		{
			get
			{				
				if (castDirection == BasicDirections.Custom)
				{
					return rotateWithTransform? (Vector2) baseTransform.TransformDirection(castDirecitonOverride) : castDirecitonOverride;			
				}
				else {
					return rotateWithTransform? (Vector2) baseTransform.TransformDirection(CommonLibUtil.DirectionToVector(castDirection)) : CommonLibUtil.DirectionToVector(castDirection);
				}
			}
		}

		RaycastHit2D[] hits;

		public bool Check ()
		{
			DoCast();
			if (cache.Count == 0) return false;
			else {
				if (responsiveCastRange) lastSuccededDistance = FindClosest(cache).distance;
				return true;
			}
		}

		///Cast and check if any of the results has a component of the type.
		public override bool Check<T> ()
		{
			DoCast();
			cache.RemoveAll(e => e.collider.GetComponent<T>() == null);
			if (cache.Count == 0) return false;		
			else {
				if (responsiveCastRange) lastSuccededDistance = FindClosest(cache).distance;
				return true;
			}
		}

		public override T[] CastAll<T> ()
		{
			DoCast();
			T[] temp = cache.SelectMany(e => e.collider.GetComponents<T>()).ToArray();
			if (temp.Length == 0) return null;
			else
			{
				return temp;
			}
		}
		
		public RaycastHit2D[] CastAll () {
			DoCast();
			if (cache.Count == 0) return null;
			else
			{
				return cache.ToArray();
			}
		}

		public RaycastHit2D[] CastFirstHitForAllOffsets ()
		{
			RaycastHit2D[] temp = new RaycastHit2D[castOffsets.Count];
			for (int i = 0; i < CastPointCoords.Length; i++)
			{
				if (filter2D.useDepth)
					temp[i] = Physics2D.Raycast(
						CastPointCoords[i],
						CastDirBakedNormalized,
						castRange,
						(filter2D.useLayerMask)? filter2D.layerMask : (LayerMask) System.Int32.MaxValue,
						filter2D.minDepth,
						filter2D.maxDepth);
				else 
					temp[i] = Physics2D.Raycast(
						CastPointCoords[i],
						CastDirBakedNormalized,
						castRange,
						(filter2D.useLayerMask)? filter2D.layerMask : (LayerMask) System.Int32.MaxValue);
			}
			return temp;
		}

		public override T Cast<T> ()
		{
			DoCast();
			cache.RemoveAll(e => e.collider.GetComponent<T>() == null);
			if (cache.Count == 0) return null;		
			else {
				RaycastHit2D r = FindClosest(cache);
				return r.collider.GetComponent<T>();
			}
		}

		public new RaycastHit2D Cast () {
			DoCast();
			if (cache.Count == 0) return new RaycastHit2D();
			else {
				RaycastHit2D r = FindClosest(cache);
				return r;
			}
		}

		public T Cast<T> (ref RaycastHit2D hit) where T : Component
		{
			DoCast();
			cache.RemoveAll(e => e.collider.GetComponent<T>() == null);
			if (cache.Count == 0)
			{
				return null;					
			}
			else
			{
				RaycastHit2D r = FindClosest(cache);
				return r.collider.GetComponent<T>();
			}
		}

		public RaycastHit2D CheckForDistance<T> (float distance) where T : Component
		{
			float temp = castRange;
			castRange = distance;
			RaycastHit2D result = new RaycastHit2D();
			Cast<T>(ref result);
			castRange = temp;
			return result;
		}

		public RaycastHit2D CheckForDistance (float distance)
		{
			float temp = castRange;
			castRange = distance;
			RaycastHit2D result = Cast();
			castRange = temp;
			return result;
		}

		RaycastHit2D FindClosest (List<RaycastHit2D> list) {
			float closestDis = list[0].distance;
			RaycastHit2D result = list[0];

			for (int i = 1; i < list.Count; i++) {
				float dis = list[i].distance;
				if (closestDis > Mathf.Abs(dis)) {
					result = list[i];
					closestDis = dis;
				}
			}

			return result;
		}
    }
}