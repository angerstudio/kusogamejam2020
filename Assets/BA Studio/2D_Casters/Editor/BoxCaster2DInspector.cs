﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BA_Studio.UnityLib.Caster2D
{
	[CustomEditor(typeof(BoxCaster2D))]
	public class BoxCaster2DInspector : PhysicsCaster2DV2Inspector {

		BoxCaster2D Target;

		Vector3[] points;

		public void OnEnable () {
			base.OnEnable();
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
				Target = (BoxCaster2D) target;
				GUILayout.Label("Caster Settings", EditorStyles.boldLabel);
				DrawCommonFields(Target);
				GUILayout.Space(8);
				GUILayout.Label("Box Settings", EditorStyles.boldLabel);
				GUILayout.BeginVertical();
					Target.castSize = EditorGUILayout.Vector2Field("Size", Target.castSize);
				GUILayout.EndVertical();        
			if (EditorGUI.EndChangeCheck()) SceneView.RepaintAll();
			serializedObject.ApplyModifiedProperties();		
		}

		public void OnSceneGUI () {
			if (Target == null) return;
			if (!Target.ToggleVisualization) return;
			if (EditorApplication.isPlaying) offsetPos.Set(Target.transform.position.x + Target.CastBaseOffsetFlipped.x, Target.transform.position.y + Target.CastBaseOffsetFlipped.y);
			else offsetPos.Set(Target.transform.position.x + Target.castBaseOffset.x, Target.transform.position.y + Target.castBaseOffset.y);
			if (points == null) points = new Vector3[5];
			points[0].Set(offsetPos.x - Target.castSize.x/2, offsetPos.y - Target.castSize.y/2, 0);
			points[1].Set(offsetPos.x + Target.castSize.x/2, offsetPos.y - Target.castSize.y/2, 0);
			points[2].Set(offsetPos.x + Target.castSize.x/2, offsetPos.y + Target.castSize.y/2, 0);
			points[3].Set(offsetPos.x - Target.castSize.x/2, offsetPos.y + Target.castSize.y/2, 0);
			points[4].Set(offsetPos.x - Target.castSize.x/2, offsetPos.y - Target.castSize.y/2, 0);
			Handles.DrawPolyLine(points);
		}
	}

}