﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using BA_Studio.UnityLib.General;
using BA_Studio.UnityLib.General.CommonEvents;


namespace BA_Studio.UnityLib.Caster2D
{
	public abstract class PhysicsCaster2DV2 : MonoBehaviour {

		[System.Serializable]
		public class CastedEvent : UnityEvent<PhysicsCaster2DV2> {

		}
		
		public string ID;

		[HideInInspector]
		public int maxCandidate = 20;		
		public ContactFilter2D filter2D;
		public Transform baseTransform;

		[TooltipAttribute("True: Ignore all objects without target tags. False: Ignore objects with ignore tags.")]
		[HideInInspector]
		public bool whiteListMode = false;

		[TooltipAttribute("Objects with these tags are prior to those not.")]
		public List<string> targetTags;

		[TooltipAttribute("Objects with these tags will be excluded first.")]
		public List<string> ignoreTags;

		[TooltipAttribute("These colliders will be excluded first.")]
		public List<Collider2D> blackList;

		public bool ToggleVisualization;

		public Vector2 castBaseOffset;

		Vector2 castBaseOffsetFlipped;

		public Vector2 CastBaseOffsetFlipped
		{
			get
			{				
				castBaseOffsetFlipped = castBaseOffset;
				if (flipX)
				{
					castBaseOffsetFlipped.x = castBaseOffset.x * -1;
				}
				
				if (flipY)
				{
					castBaseOffsetFlipped.y = castBaseOffset.y * -1;
				}
				return castBaseOffsetFlipped;
			}
		}

		public bool flipX, flipY;

		public LayerMaskEvent maskChanged;

		public static T GetCasterByID<T> (GameObject Parent, string ID) where T : PhysicsCaster2DV2
		{
			if (Parent.GetComponent<T>() == null || !Parent.GetComponents<T>().Any(e => e.ID == ID))
			{
				Debug.LogError("Can't find a " + typeof(T).Name + " with ID: \"" + ID + "\" on " + Parent.name);
				return null;
			}
			return Parent.GetComponents<T>().First(e => e.ID == ID);
		}

		public static T GetCasterByIDInChildren<T> (GameObject parent, string ID) where T: PhysicsCaster2DV2
		{
			if (parent.GetComponentInChildren<T>() == null || !parent.GetComponentsInChildren<T>().Any(e => e.ID == ID)) {
				Debug.LogError("Can't find a " + typeof(T).Name + " with ID: \"" + ID + "\" on " + parent.name + " or its children.");
				return null;
			}
			return parent.GetComponentsInChildren<T>().First(e => e.ID == ID);
		}

		public virtual void Start ()
		{
			if (baseTransform == null) baseTransform = this.transform;
		}

		void UpdateCastBaseOffsetFlipped ()
		{
		}

		public virtual void Update () {}

		public abstract bool Check<T> () where T : Component;
		public abstract T Cast<T> () where T : Component;
		public abstract T[] CastAll<T> () where T : Component;
		public virtual Collider2D Cast () {
			return Cast<Collider2D>();
		}

		protected virtual void PreCast ()
		{
			UpdateCastBaseOffsetFlipped();
		}

		// Acutal casting process
		protected virtual void DoCast () {
			PreCast();
			//Casting
			DoneCast();
		}

		protected virtual void DoneCast ()
		{
		}

		public void SetMask (LayerMask Mask) {
			filter2D.layerMask = Mask;
			if (maskChanged != null) maskChanged.Invoke(Mask);
		}
	}
}

