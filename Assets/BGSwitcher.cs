﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGSwitcher : MonoBehaviour
{
    float lastUpdateTime;

    public Sprite[] backgrounds;
    public SpriteRenderer spriteRenderer;
    int index;

    public float interval = 5;

    void Update ()
    {
        if (Time.time - lastUpdateTime >= interval)
        {
            lastUpdateTime = Time.time;
            index++;
            if (index == backgrounds.Length) index = 0;
            spriteRenderer.sprite = backgrounds[index];
        }
        
    }
}
