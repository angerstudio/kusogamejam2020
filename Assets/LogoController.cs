﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class LogoController : MonoBehaviour
{
    LogoControl logoControlInput;

    public CanvasGroup canvasGroup;

    public UnityEvent onStart;

    void Awake ()
    {
        logoControlInput = new LogoControl();
        logoControlInput.Logo.Anykey1.performed += ctx => 
        {
            onStart.Invoke();
            canvasGroup.DOFade(0, 0.4f).onComplete += () => {
                Destroy(this);
            };
        };
    }

    void OnEnable ()
    {
        logoControlInput.Enable();
    }

    void OnDisable ()
    {
        logoControlInput.Disable();
    }
}
