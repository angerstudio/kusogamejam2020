﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class MemePlayer : MonoBehaviour
{
    [SerializeField]
    float SpritePlayTime = 5;
    [SerializeField]
    VideoPlayer videoPlayer;
    [SerializeField]
    RenderTexture renderTexture;
    [SerializeField]
    AudioSource audioSource;
    [SerializeField]
    GameObject rawImage;
    [SerializeField]
    Image MemeImage;

    void Awake()
    {
        videoPlayer.prepareCompleted += onVideoPrepared;
        videoPlayer.loopPointReached += onVideoEnded;
    }

    

    public void onResultReceived(ResultObject result)
    {
        switch(result.type)
        {
            case ResultType.VIDEO:
                PlayVideoMeme(result.clip);
                break;
            case ResultType.SPRITE_SOUND:
                PlayAudioImageMeme(result.sprite, result.audio);
                break;
            case ResultType.SPRITE:
                PlayImageMeme(result.sprite);
                break;
        }
    }


    IEnumerator wait(float time)
    {
        yield return new WaitForSeconds(time);
        MemeImage.gameObject.SetActive(false);
        Core.Singleton.pauseInput = false;
    }

    void ShowSprite(Sprite sprite)
    {
        MemeImage.sprite = sprite;
        MemeImage.gameObject.SetActive(true);
    }

    Coroutine cImage, cAImage;
    public void PlayImageMeme (Sprite sprite)
    {
        Core.Singleton.pauseInput = true;
        if (cImage != null) StopCoroutine(cImage);
        ShowSprite(sprite);
        cImage = StartCoroutine(wait(SpritePlayTime));
    }

    public void PlayAudioImageMeme(Sprite sprite, AudioClip audio)
    {
        Core.Singleton.pauseInput = true;
        if (cAImage != null) StopCoroutine(cAImage);
        audioSource.Pause();
        ShowSprite(sprite);
        audioSource.PlayOneShot(audio);
        cAImage = StartCoroutine(wait(audio.length));
    }

    public void PlayVideoMeme (VideoClip clip)
    {
        Core.Singleton.pauseInput = true;
        videoPlayer.clip = clip;
        videoPlayer.Prepare();
    }

    void onVideoPrepared(VideoPlayer player)
    {
        player.Play();
        rawImage.SetActive(true);
    }

    void onVideoEnded(VideoPlayer player)
    {
        rawImage.SetActive(false);
        player.Stop();
        Core.Singleton.pauseInput = false;
    }

    public void ShowRecipt (ResultObject obj)
    {
        
    }
}
